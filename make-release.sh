#!/bin/bash

# Copyright © 2021 rusty-snake
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -euo pipefail

# cd into the project directory
cd -P -- "$(readlink -e "$(dirname "$0")")"

# Do not run if an old outdir exists
[ -d outdir ] && { echo "Please delete 'outdir' first."; exit 1; }

# Check presents of non-standard programs (everything except coreutils and built-ins)
if ! command -v cargo >&-; then
	echo "make-release.sh: Missing requirement: cargo is not installed or could not be found."
	echo "Please make sure cargo is installed and in \$PATH."
	exit 1
fi
if ! command -v git >&-; then
	echo "make-release.sh: Missing requirement: git is not installed or could not be found."
	echo "Please make sure git is installed and in \$PATH."
	exit 1
fi
if ! command -v podman >&-; then
	echo "make-release.sh: Missing requirement: podman is not installed or could not be found."
	echo "Please make sure podman is installed and in \$PATH."
	exit 1
fi
if ! command -v xz >&-; then
	echo "make-release.sh: Missing requirement: xz is not installed or could not be found."
	echo "Please make sure xz is installed and in \$PATH."
	exit 1
fi

# Pull alpine image if necessary
if [[ -z "$(podman image list --noheading alpine:latest)" ]]; then
	podman pull docker.io/library/alpine:latest
fi

# Check if we are allowed to run podman
if [[ "$(podman run --rm alpine:latest echo "hello")" != "hello" ]]; then
	echo "make-release.sh: podman does not seem to work correctly."
	exit 1
fi

IFS='#' read -r PROJECT VERSION < <(basename "$(cargo pkgid)")

# Vendor all dependencies
cargo vendor --color=never --locked vendor
[ -d .cargo ] && mv -v .cargo .cargo.bak
mkdir -v .cargo
trap "rm -rv .cargo && [ -d .cargo.bak ] && mv -v .cargo.bak .cargo" EXIT
echo "[ INFO ] Create .cargo/config.toml"
cat > .cargo/config.toml <<EOF
[source.crates-io]
replace-with = "vendored-sources"
[source.vendored-sources]
directory = "vendor"
EOF

mkdir -v outdir

echo "[ INFO ] Pack source archive"
git archive --format=tar --prefix="$PROJECT-$VERSION/" -o "outdir/$PROJECT-$VERSION.src.tar" HEAD
tar --xform="s,^,$PROJECT-$VERSION/," -rf "outdir/$PROJECT-$VERSION.src.tar" .cargo vendor
xz "outdir/$PROJECT-$VERSION.src.tar"
echo "[ INFO ] Finish"

echo "[ INFO ] Build binary archive"
BUILDDIR="/builddir"
INSTALLDIR="/installdir"
podman run --rm --security-opt=no-new-privileges --cap-drop=all \
	-v ./outdir:/outdir:z --tmpfs "$BUILDDIR" --tmpfs "$INSTALLDIR:mode=0755" \
	-w "$BUILDDIR" alpine:latest sh -euo pipefail -c "
		apk update
		apk upgrade ||:
		apk add curl gcc make musl-dev openssl-dev perl pkgconf xz ||:
		curl --proto '=https' --tlsv1.3 -sSf 'https://sh.rustup.rs' | sh -s -- -y --profile minimal
		source ~/.cargo/env
		tar --strip=1 -xf '/outdir/$PROJECT-$VERSION.src.tar.xz'
		cargo build --release --frozen
		strip ./target/release/uhb2dnsmasq
		install -Dm0755 ./target/release/uhb2dnsmasq '$INSTALLDIR/usr/local/bin/uhb2dnsmasq'
		install -Dm0644 ./systemd/uhb2dnsmasq.service '$INSTALLDIR/usr/local/lib/systemd/system/uhb2dnsmasq.service'
		install -Dm0644 ./LICENSE '$INSTALLDIR/usr/local/share/licenses/uhb2dnsmasq/LICENSE'
		tar -C '$INSTALLDIR' -cJf '/outdir/$PROJECT-$VERSION-x86_64-unknown-linux-musl.tar.xz' .
	"
echo "[ INFO ] Finish"

# Compute checksums
sha256sum outdir/*.tar.xz > outdir/SHA256SUMS
sha512sum outdir/*.tar.xz > outdir/SHA512SUMS
echo "Success!"
