/*
 * Copyright © 2020-2023 rusty-snake
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#[cfg(feature = "download")]
use once_cell::sync::Lazy;
#[cfg(feature = "download")]
use std::thread;
#[cfg(feature = "download")]
use std::time;

#[cfg(feature = "download")]
pub fn wait_for_inet(opt: &crate::cli::Args) {
    use thread::sleep;
    use time::Duration;

    if let Some(url) = opt.connectivity_url.as_ref() {
        log::debug!("Testing internet connectivity");

        let client = reqwest::blocking::Client::new();
        while client.get(url).send().is_err() {
            log::info!("No internet connection, retry in 5min.");
            sleep(Duration::from_secs(300));
        }
    } else {
        log::debug!("Checking internet connectivity using NetworkManager");

        while zbus::blocking::Connection::system()
            .expect("Failed to connect to the system bus")
            .call_method(
                Some("org.freedesktop.NetworkManager"),
                "/org/freedesktop/NetworkManager",
                Some("org.freedesktop.DBus.Properties"),
                "Get",
                &("org.freedesktop.NetworkManager", "Connectivity"),
            )
            .expect("Failed to check connectivity using NetworkManager")
            .body()
            .deserialize::<u32>()
            .expect("Failed to check connectivity using NetworkManager")
            // 4 = full: The host is connected to a network and has full access to the Internet.
            .ne(&4)
        {
            log::info!("No internet connectivity, retry in 5min.");
            sleep(Duration::from_secs(300));
        }
    }
    log::debug!("Confirmed internet connectivity");
}

#[cfg(feature = "download")]
pub async fn download(url: &str) -> Result<String, reqwest::Error> {
    static CLIENT: Lazy<reqwest::Client> = Lazy::new(|| {
        reqwest::Client::builder()
            .user_agent(crate::USER_AGENT)
            .min_tls_version(reqwest::tls::Version::TLS_1_2)
            .https_only(true)
            .build()
            .expect("Failed to build the http client.")
    });

    log::debug!("Start download of '{}'.", url);
    let response = CLIENT.get(url).send().await?.text().await?;
    log::debug!("Finish download of '{}'.", url);
    Ok(response)
}
