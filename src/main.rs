/*
 * Copyright © 2020-2023 rusty-snake
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#![warn(rust_2018_idioms)]
#![deny(missing_debug_implementations)]

use clap::Parser;
use log::{debug, warn};
use std::fs;
use std::fs::File;
use std::io;
use std::io::BufWriter;
use stderrlog::StdErrLog;

use types::{Hosts, Output};
#[cfg(feature = "download")]
use utils::{download, wait_for_inet};

mod cli;
mod parsers;
mod types;
mod utils;
mod writers;

#[cfg(feature = "download")]
const USER_AGENT: &str = concat!(
    "uhb2dnsmasq/",
    env!("CARGO_PKG_VERSION"),
    " ",
    "reqwest/",
    env!("REQWEST_VERSION")
);

fn main() {
    let args = cli::Args::parse();

    StdErrLog::new()
        .verbosity(args.verbose as usize + 2) // info as default
        .quiet(args.quiet)
        .show_level(true)
        .timestamp(args.timestamp())
        .color(args.color_choice())
        .module(module_path!())
        .show_module_names(false)
        .init()
        .unwrap();

    #[cfg(feature = "download")]
    if !args.no_connectivity_check {
        // Wait until internet connectivity is proved.
        wait_for_inet(&args);
    }

    tokio::runtime::Runtime::new()
        .expect("Failed to create the tokio runtime.")
        .block_on(uhb2dnsmasq(args))
        .unwrap();
}

async fn uhb2dnsmasq(args: cli::Args) -> anyhow::Result<()> {
    let domains_list = if let Some(path) = args.uhb.strip_prefix("file://") {
        fs::read_to_string(path)?
    } else {
        #[cfg(not(feature = "download"))]
        panic!("uhb2dnsmasq was build without download support.");

        #[cfg(feature = "download")]
        download(&args.uhb).await?
    };

    let hosts = parsers::domains_list(&domains_list).collect::<Hosts>();

    let (stdout, mut locked_stdout, output_file, mut buffered_output_file);
    let out: Output<'_> = if args.output_file == "-" {
        stdout = io::stdout();
        locked_stdout = stdout.lock();
        &mut locked_stdout
    } else {
        output_file = File::create(&args.output_file)?;
        buffered_output_file = BufWriter::with_capacity(512 * 1024 /* 512kb */, output_file);
        &mut buffered_output_file
    };

    debug!("Start writing");
    match &*args.output_format {
        "dnsmasq:null" => writers::dnsmasq_null_response(&hosts, out)?,
        "dnsmasq:nxdomain" => writers::dnsmasq_nxdomain_response(&hosts, out)?,
        _ => unreachable!(),
    };
    debug!("Finished writing");

    out.flush()?;

    Ok(())
}
