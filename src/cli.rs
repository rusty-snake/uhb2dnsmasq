/*
 * Copyright © 2020-2023 rusty-snake
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

use clap::{ArgAction, Parser};
use std::io::{self, IsTerminal};
use std::str::FromStr;
use termcolor::ColorChoice;

#[derive(Parser, Debug)]
#[clap(about)]
pub struct Args {
    /// Be quiet
    #[clap(short = 'q', long)]
    pub quiet: bool,

    /// Be verbose (-v, -vv)
    #[clap(short = 'v', long, action = ArgAction::Count)]
    pub verbose: u8,

    /// Add timestamps to logging output
    #[clap(
        short = 't',
        long,
        visible_alias = "ts",
        default_value = "off",
        value_parser = ["sec", "ms", "us", "ns", "none", "off"],
    )]
    timestamp: String,

    /// Specify when to use colored output
    #[clap(
        long = "color",
        default_value = "auto",
        value_parser = ["always", "ansi", "auto", "never"],
    )]
    color_choice: String,

    #[cfg(feature = "download")]
    /// Do not perform any connectivity checks; assume connectivity
    #[clap(long)]
    pub no_connectivity_check: bool,

    #[cfg(feature = "download")]
    /// Connect to this url to proof connectivity instead of asking the
    /// NetworkManager
    #[clap(long, value_name = "URL")]
    pub connectivity_url: Option<String>,

    /// The url from which to download the Ultimate.Hosts.Blacklist;
    /// supported are http://, https:// and file://
    #[clap(
        long,
        value_name = "URL",
        default_value = "https://hosts.ubuntu101.co.za/domains.list"
    )]
    pub uhb: String,

    /// The output format
    #[clap(
        short = 'f',
        long,
        value_parser = ["dnsmasq:null", "dnsmasq:nxdomain"],
    )]
    pub output_format: String,

    /// Path where to write the conf-file; use "-" for stdout
    #[clap(short = 'o', long, visible_alias = "output")]
    pub output_file: String,
}
impl Args {
    pub fn color_choice(&self) -> ColorChoice {
        match self.color_choice.as_str() {
            "always" => ColorChoice::Always,
            "ansi" => ColorChoice::AlwaysAnsi,
            "auto" => {
                if io::stdout().is_terminal() {
                    ColorChoice::Auto
                } else {
                    ColorChoice::Never
                }
            }
            "never" => ColorChoice::Never,
            _ => unreachable!(),
        }
    }

    pub fn timestamp(&self) -> stderrlog::Timestamp {
        stderrlog::Timestamp::from_str(&self.timestamp).unwrap()
    }
}
