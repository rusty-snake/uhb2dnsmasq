/*
 * Copyright © 2020-2023 rusty-snake
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

use crate::types::{Hosts, Output};
use std::io::Result as IoResult;

pub fn dnsmasq_null_response(hosts: &Hosts, output: Output<'_>) -> IoResult<()> {
    dnsmasq_impl(hosts, output, b"/#\n")
}

pub fn dnsmasq_nxdomain_response(hosts: &Hosts, output: Output<'_>) -> IoResult<()> {
    dnsmasq_impl(hosts, output, b"/\n")
}

fn dnsmasq_impl(hosts: &Hosts, output: Output<'_>, addr_opt_end: &'static [u8]) -> IoResult<()> {
    for host in hosts.keys() {
        log::trace!("Processing hostname '{}'.", host);

        if is_redundant(host, hosts) {
            log::trace!("Skipping redundant hostname '{}'.", host);
            continue;
        }

        output.write_all(b"address=/")?;
        output.write_all(host.as_bytes())?;
        output.write_all(addr_opt_end)?;
    }

    Ok(())
}

fn is_redundant(host: &str, hosts: &Hosts) -> bool {
    let domain = &hosts[host];
    hosts.contains_key(domain) && host != *domain
}
