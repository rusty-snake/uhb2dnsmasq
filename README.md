# uhb2dnsmasq

Download the [Ultimative.Hosts.Blacklist](https://github.com/Ultimate-Hosts-Blacklist/Ultimate.Hosts.Blacklist) and convert it into a dnsmasq configuration.

## Getting started

First install [rust](https://www.rust-lang.org/tools/install), openssl development files and git.

Fedora command:

```
$ sudo dnf install rust cargo git openssl-devel
```

Now you can download the source code, build it and install.

```
$ git clone https://gitlab.com/rusty-snake/uhb2dnsmasq.git
$ cd uhb2dnsmasq
$ export RUSTFLAGS=" -Ctarget-cpu=native"
$ cargo build --release
$ sudo install -Dm0755 target/release/uhb2dnsmasq /usr/local/bin/uhb2dnsmasq
```

Start the download and convert it. Maybe you want to add `--nxdomain` to create a
conf file which response with `NXDOMAIN` instead of `NULL`.

```
$ uhb2dnsmasq blacklist.conf
$ sudo mv blacklist.conf /etc/dnsmasq.d/blacklist.conf
```

You can also use a systemd service for that. Note that you maybe need to adapt it.

```
$ sudo install -Dm0644 systemd/uhb2dnsmasq.service /etc/systemd/system/uhb2dnsmasq.service
$ sudo systemctl daemon-reload
$ sudo systemctl enable --now uhb2dnsmasq.service
```
